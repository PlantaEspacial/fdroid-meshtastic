repo_url = "https://plantaespacial.frama.io/fdroid-meshtastic/fdroid/"
repo_name = "Meshtastic F-Droid repo (unofficial)"
repo_icon = "fdroid-icon.png"
repo_description = """
Unofficial Meshtastic repo. (The .apk files come from https://github.com/meshtastic/Meshtastic-Android)
"""

archive_older = 1

local_copy_dir = "/fdroid"

keystore = "../keystore.bks"
repo_keyalias = "PlantaEspacial"
